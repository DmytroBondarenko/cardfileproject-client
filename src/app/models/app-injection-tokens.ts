import { InjectionToken } from '@angular/core'

export const CARD_FILE_API_URL = new InjectionToken<string>('Card file api url');