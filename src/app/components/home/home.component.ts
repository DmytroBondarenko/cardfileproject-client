import { Component, OnInit } from '@angular/core';
import { TextService } from 'src/app/services/text-service/text.service';
import { Text } from 'src/app/interfaces/text'
import { FormControl, FormGroup } from '@angular/forms';
import { Genre } from 'src/app/interfaces/genre';
import { GenreService } from 'src/app/services/genre-service/genre.service';
 
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  constructor(private textService: TextService, 
    private genreService: GenreService) { }

  texts: Text[] = [];

  genresList: Genre[] = [];

  searchOrders: string[] = ["Ascending", "Descending"]

  searchForm!: FormGroup;
  
  ngOnInit(): void {
    this.genreService.getGenres().subscribe((genres) => {
      this.genresList = genres;
    });
    this.textService.getAllTexts().subscribe((data) => {
      this.texts = data;
    });
    this.searchForm = new FormGroup({
      search: new FormControl(),
      genres: new FormControl(),
      sortOrder: new FormControl()
    });
  }

  search(){
    let searchConditions = this.searchForm.value;
    console.log(searchConditions);
    this.textService.getAllTexts().subscribe((data) => {
      this.texts = data;
      if(searchConditions.search){
        this.texts = this.texts.filter(text => {
          return text.title.toLowerCase().includes((searchConditions.search + "").toLowerCase());
        });
      }
      if(searchConditions.genres){
        if((searchConditions.genres as string[]).length !== 0){
          let genreIdsList : string[] = [];
          searchConditions.genres.forEach((genre : string) => {
            genreIdsList.push(this.genresList.find(i => i.genreName === genre)?.id!);
          });
          this.texts.forEach(text => {
            if(!text.genreIds.some(genre => genreIdsList.includes(genre))){
              this.texts = this.texts.filter(textToDelete => {
                return textToDelete.id !== text.id;
              })
            }
          });
        }
      }
      if(searchConditions.sortOrder){
        if(searchConditions.sortOrder === "Ascending"){
          this.texts.sort((a, b) => (a.rating > b.rating) ? 1 : -1)
        }
        else{
          this.texts.sort((a, b) => (a.rating < b.rating) ? 1 : -1)
        }
      }
    });
  }
}
