import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Roles } from 'src/app/constants/Roles';
import { ApiError } from 'src/app/helpers/api-error';
import { Genre } from 'src/app/interfaces/genre';
import { User } from 'src/app/interfaces/user';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { GenreService } from 'src/app/services/genre-service/genre.service';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  usersList!: User[];

  genreList!: Genre[];

  user!: User;

  tempUser!: User;

  genreAddForm!: FormGroup;

  genreAddError!: string;

  tableMode: boolean = true;
  
  constructor(private userService: UserService, 
    private authService: AuthService,
    private genreService: GenreService) { }

  ngOnInit(): void {
    this.userService.getAllUserProfiles().subscribe(users => {
      this.usersList = users;
      this.usersList = this.usersList.filter(user => user.id !== this.authService.getUserId());
    });
    this.genreService.getGenres().subscribe(genres => {
      this.genreList = genres;
    });

    this.genreAddError = '';
    this.genreAddForm = new FormGroup({
      genreName: new FormControl()
    });
  }

  editUser(u: User) {
    this.tempUser = {
      id: u.id,
      userName: u.userName,
      lastName: u.lastName,
      textIds: u.textIds,
      email: u.email,
      firstName: u.firstName
    };
    this.user = u;
  }

  cancelGenre() {
    this.tableMode = true;
  }

  cancelUser() {
    this.user.email = this.tempUser.email;
    this.user.firstName = this.tempUser.firstName;
    this.user.lastName = this.tempUser.lastName;
    this.user.userName = this.tempUser.userName;
    this.user = {} as User;
  }

  add() {
    this.tableMode = false;
  }

  addNewGenre(){
    this.genreService.addGenre(this.genreAddForm.value).subscribe(() => {
      alert("Successfully added!");
      window.location.reload();
    }, 
    exc => {
      this.genreAddError = ApiError.StringBuilder(exc);
    });
  }

  updateUser(){
    this.userService.updateUserById(this.user, this.user.id).subscribe(
      () => {
        alert("Update successfully completed!");
        window.location.reload();
      },
      (exc) => {
        alert(ApiError.StringBuilder(exc));
      }
    );
  }

  deleteGenre(genre: Genre){
    if(confirm("Are you sure want to delete this genre?")) {
      this.genreService.deleteGenre(genre.id).subscribe(() => {
        window.location.reload();
      }, error => {
        alert(ApiError.StringBuilder(error));
      });
    }
  }

  deleteUser(user: User){
    if(confirm("Are you sure want to delete this user?")) {
      this.userService.deleteUserById(user.id).subscribe(() => {
        window.location.reload();
      }, error => {
        alert(ApiError.StringBuilder(error));
      })
    }
  }

  makeAdmin(){
    if(confirm("Are you sure want to make this user admin?")) {
      this.userService.addUserToRole(this.user.id, Roles.Admin).subscribe(() => {
        alert("Success!");
        window.location.reload();
      }, error => {
        alert(ApiError.StringBuilder(error));
      })
    }
  }
}
