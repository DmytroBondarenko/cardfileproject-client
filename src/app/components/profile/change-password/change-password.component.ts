import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiError } from 'src/app/helpers/api-error';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  constructor(private userService: UserService,
    private authSerivce: AuthService,
    private router: Router) { }
  
  changePasswordForm!: FormGroup;

  changePasswordError!: string;

  ngOnInit(): void {
    this.changePasswordError = '';
    this.changePasswordForm = new FormGroup({
      currentPassword: new FormControl(),
      newPassword: new FormControl(),
    });
  }

  changePassword(){
    this.userService.changePassword(this.changePasswordForm.value).subscribe(() => {
      this.authSerivce.logout();
      alert("Password successfully changed!")
      this.router.navigate(['/login']);
    }, (exc) => {
      this.changePasswordError = ApiError.StringBuilder(exc);
    });
  }
}
