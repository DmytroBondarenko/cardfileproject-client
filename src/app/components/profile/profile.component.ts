import { Component, OnInit } from '@angular/core';
import { TextService } from 'src/app/services/text-service/text.service';
import { Text } from 'src/app/interfaces/text'
import { UserService } from 'src/app/services/user-service/user.service';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { Router } from '@angular/router';
import { ApiError } from 'src/app/helpers/api-error';
import { Roles } from 'src/app/constants/Roles';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private textService: TextService, 
    private userService: UserService, 
    private authService: AuthService,
    private router: Router) {}
  
  texts: Text[] = [];

  firstName!: string;

  lastName!: string;

  updateForm!: FormGroup;
  
  updateError!: string;

  isAdmin: boolean = false;
  
  ngOnInit(): void {
    this.textService.getYourTexts().subscribe((data) => {
      this.texts = data;
    });

    this.userService.getYourProfile().subscribe((profile) => {
      this.firstName = profile.firstName;
      this.lastName = profile.lastName;
      this.updateForm.setValue({
        firstName: profile.firstName,
        lastName: profile.lastName,
        userName: profile.userName,
        email: profile.email
      });
    });

    this.updateError = '';
    this.updateForm = new FormGroup({
      firstName: new FormControl(),
      lastName: new FormControl(),
      userName: new FormControl(),
      email: new FormControl(),
    });

    this.userService.getRoles().subscribe(roles => {
      if(roles.includes(Roles.Admin)){
        this.isAdmin = true;
      }
    })

  }

  update(){
    this.userService.updateYourProfile(this.updateForm.value).subscribe(
      () => {
        alert("Update successfully completed!");
      },
      (exc) => {
        this.updateError = ApiError.StringBuilder(exc);
      }
    );
  }

  delete() {
    if(confirm("Are you sure want to delete your account?")) {
      this.userService.deleteUser().subscribe(() => {
        this.authService.logout();
        this.router.navigate(['']);
      });
    }
  }

  addText(){
    this.router.navigate(["/add-text"]);
  }

  changePassword(){
    this.router.navigate(["/change-password"]);
  }

  adminPanel(){
    this.router.navigate(["/admin-panel"]);
  }
}
