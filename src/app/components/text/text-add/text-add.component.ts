import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Genre } from 'src/app/interfaces/genre';
import { GenreService } from 'src/app/services/genre-service/genre.service';
import { TextAdd } from 'src/app/interfaces/text-add';
import { TextService } from 'src/app/services/text-service/text.service';
import { Router } from '@angular/router';
import { ApiError } from 'src/app/helpers/api-error';

@Component({
  selector: 'app-text-add',
  templateUrl: './text-add.component.html',
  styleUrls: ['./text-add.component.css']
})
export class TextAddComponent implements OnInit {

  constructor(private genreService: GenreService, 
    private textService: TextService,
    private router: Router) { }
  
  genresList: Genre[] = [];

  addTextForm!: FormGroup;

  addTextError!: string;

  ngOnInit(): void {
    this.genreService.getGenres().subscribe((genres) => {
      this.genresList = genres;
    });
    this.addTextError = '';
    this.addTextForm = new FormGroup({
      title: new FormControl(),
      body: new FormControl(),
      genres: new FormControl()
    });
  }

  addText(){
    let textToAdd = this.addTextForm.value;
    let genreIdsList :string[] = []
    textToAdd.genres.forEach((genre: string) => {
      genreIdsList.push(this.genresList.find(i => i.genreName === genre)?.id!);
    });
    let newText: TextAdd = {
      title: textToAdd.title,
      body: textToAdd.body,
      genreIds: genreIdsList
    }
    this.textService.addText(newText).subscribe(() => {
      this.router.navigate(['/profile']);
    },
    (exc) => {
      this.addTextError = ApiError.StringBuilder(exc);
    });
  }
}
