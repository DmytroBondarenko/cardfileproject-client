import { Component, Input, OnChanges } from '@angular/core';
import { GenreService } from 'src/app/services/genre-service/genre.service';
import { UserService } from 'src/app/services/user-service/user.service';
import { Text } from 'src/app/interfaces/text'
import { DatePipe } from '@angular/common';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { Roles } from 'src/app/constants/Roles';
import { TextService } from 'src/app/services/text-service/text.service';

@Component({
  selector: 'app-text-item',
  templateUrl: './text-item.component.html',
  styleUrls: ['./text-item.component.css']
})
export class TextItemComponent implements OnChanges {
  
  constructor(public datepipe: DatePipe, 
    private authService: AuthService, 
    private genreService: GenreService, 
    private textSerivce: TextService, 
    private userService: UserService) 
    { }

  @Input() item!: Text;  
    
  genreNames: string = '';

  userName!: string;

  date!: string;

  isUserOwner: boolean = false;

  canLike: boolean = false;

  ngOnChanges():void{
    this.item.genreIds.forEach((element) => {
      this.genreService.getGenreById(element).subscribe((genre) => {
        this.genreNames = this.genreNames + genre.genreName + "; ";
      })
    });
    this.userService.getUserById(this.item.userId).subscribe((user) => {
      this.userName = user.userName;
      if(this.authService.isAuthenticated()){
        this.canLike = true;
        this.userService.getRoles().subscribe(roles => {
          if(roles.indexOf(Roles.Admin) > -1){
            this.isUserOwner = true;
          }
          else if(user.id === this.authService.getUserId()){
            this.isUserOwner = true;
          }
        });
      }
    });
    this.date = this.datepipe.transform(this.item.publishDate, 'dd/MM/yyyy')!;
  }

  like(){
    this.textSerivce.rateText(this.item.id).subscribe(
      () => {
        this.item.rating++;
      }, 
      (exc) => {
        alert(exc.error.message);
      });
  }

  delete(){
    this.textSerivce.deleteText(this.item.id).subscribe(()=>{
      window.location.reload();
    });
  }
}
