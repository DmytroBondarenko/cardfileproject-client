import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth-service/auth.service';
import { UserService } from './services/user-service/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  
  constructor(private authService: AuthService, userService: UserService, private router: Router) {
    if(this.isLoggedIn){
      userService.getUserById(this.authService.getUserId()).subscribe((user) => {
        this.userName = user.userName;
        this.firstName = user.firstName;
        this.lastName = user.lastName;
      });
    }
  }
  
  title = 'Card File';

  userName: string = '';

  firstName: string = '';

  lastName : string = '';

  public get isLoggedIn() : boolean {
    return this.authService.isAuthenticated();
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['']).then(() => {
      window.location.reload();
    });
  }
}
