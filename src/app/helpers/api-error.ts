export class ApiError {
    static StringBuilder(exc: any): string{
        let errorMessage = "";
        if(exc.error.errors !== undefined){
          for (const [key, value] of Object.entries(exc.error.errors)) {
            errorMessage = errorMessage + value + " ";
          }
        }
        else{
            errorMessage = exc.error.message
        }
        return errorMessage;
    }
  }