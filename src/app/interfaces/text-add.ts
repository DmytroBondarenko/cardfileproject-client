export interface TextAdd {
    title : string;
    body : string;
    genreIds : string[];
}
