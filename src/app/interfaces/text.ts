export interface Text {
    id : string;
    userId : string;
    title : string;
    body : string;
    rating : number;
    publishDate : Date;
    genreIds : string[];
}
