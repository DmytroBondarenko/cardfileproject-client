import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Text } from 'src/app/interfaces/text'
import { CARD_FILE_API_URL } from 'src/app/models/app-injection-tokens';
import { TextAdd } from 'src/app/interfaces/text-add';

@Injectable({
  providedIn: 'root'
})
export class TextService {
  
  constructor(private http: HttpClient, 
    @Inject(CARD_FILE_API_URL) private apiUrl: string) { }

  getAllTexts():Observable<Text[]>{
    return this.http.get<Text[]>(`${this.apiUrl}/api/Text`);
  }

  getTextById(id : string):Observable<Text>{
    return this.http.get<Text>(`${this.apiUrl}/api/Text/${id}`);
  }

  getAllTextsAscending():Observable<Text[]>{
    return this.http.get<Text[]>(`${this.apiUrl}/api/Text/SortByAscending`);
  }

  getAllTextsDescending():Observable<Text[]>{
    return this.http.get<Text[]>(`${this.apiUrl}/api/Text/SortByDescending`);
  }

  getAllTextsByUserName(userName : string):Observable<Text[]>{
    return this.http.get<Text[]>(`${this.apiUrl}/api/Text/UserTexts/${userName}`);
  }

  getYourTexts():Observable<Text[]>{
    return this.http.get<Text[]>(`${this.apiUrl}/api/Text/GetYourTexts`);
  }

  getAllTextsByGenreName(genreName: string):Observable<Text[]>{
    return this.http.get<Text[]>(`${this.apiUrl}/api/Text/GetByGenre?name=${genreName}`);
  }

  rateText(textId: string):Observable<any>{
    return this.http.post(`${this.apiUrl}/api/Text/RateText?textId=${textId}`, null);
  }

  addText(text: TextAdd):Observable<Text>{
    return this.http.post<Text>(`${this.apiUrl}/api/Text/AddText`, text);
  }

  deleteText(textId: string):Observable<any>{
    return this.http.delete(`${this.apiUrl}/api/Text/DeleteText/${textId}`);
  }
}
