import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ChangePassword } from 'src/app/interfaces/change-password';
import { UpdateUser } from 'src/app/interfaces/update-user';
import { User } from 'src/app/interfaces/user';
import { CARD_FILE_API_URL } from 'src/app/models/app-injection-tokens';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, 
    @Inject(CARD_FILE_API_URL) private apiUrl: string) { }

  
  getUserById(id : string):Observable<User>{
    return this.http.get<User>(`${this.apiUrl}/api/User/${id}`);
  }

  getRoles():Observable<string[]>{
    return this.http.get<string[]>(`${this.apiUrl}/api/User/GetYourRoles`);
  }

  getYourProfile():Observable<User>{
    return this.http.get<User>(`${this.apiUrl}/api/User/GetYourProfile`);
  }

  updateYourProfile(user: UpdateUser):Observable<User>{
    return this.http.put<User>(`${this.apiUrl}/api/User/UpdateUser`, user);
  }

  getAllUserProfiles():Observable<User[]>{
    return this.http.get<User[]>(`${this.apiUrl}/api/User/GetAllUserProfiles`);
  }

  getFullUserById(id : string):Observable<User>{
    return this.http.get<User>(`${this.apiUrl}/api/User/${id}/Full`);
  }

  addUserToRole(userId: string, roleName: string):Observable<any>{
    return this.http.post(`${this.apiUrl}/api/User/AddToRole/${userId}/${roleName}`, null);
  }

  updateUserById(user: UpdateUser, userId: string):Observable<User>{
    return this.http.put<User>(`${this.apiUrl}/api/User/UpdateUser/${userId}`, user);
  }

  changePassword(passwordModel: ChangePassword):Observable<any>{
    return this.http.put(`${this.apiUrl}/api/User/ChangePassword`, passwordModel);
  }

  deleteUser():Observable<any>{
    return this.http.delete(`${this.apiUrl}/api/User/DeleteUser`);
  }

  deleteUserById(userId: string):Observable<any>{
    return this.http.delete(`${this.apiUrl}/api/User/DeleteUser/${userId}`);
  }
}
