import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { Token } from 'src/app/interfaces/token';
import { CARD_FILE_API_URL } from 'src/app/models/app-injection-tokens';
import { tap } from 'rxjs/operators';
import { Register } from 'src/app/interfaces/register';

export const ACCESS_TOKEN_KEY = 'cardfile_access_token';
export const USER_ID = 'user_id';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    @Inject(CARD_FILE_API_URL) private apiUrl: string,
    private jwtHelper: JwtHelperService
  ) { }

  login(email: string, password: string): Observable<Token> {
    return this.http.post<Token>(`${this.apiUrl}/api/Auth/login`, {
      email, password
    }).pipe(
      tap(token => {
        localStorage.setItem(ACCESS_TOKEN_KEY, token.token);
        localStorage.setItem(USER_ID, token.userId);
      })
    )
  }

  isAuthenticated(): boolean {
    var token = localStorage.getItem(ACCESS_TOKEN_KEY);
    if(token){
      return !this.jwtHelper.isTokenExpired(token)
    }
    return false;
  }

  getUserId(): string {
    var id = localStorage.getItem(USER_ID);
    if(id){
      return id;
    }
    return '';
  }

  getUserToken(): string {
    var token = localStorage.getItem(ACCESS_TOKEN_KEY);
    if(token){
      return token;
    }
    return '';
  }

  logout(): void{
    localStorage.removeItem(ACCESS_TOKEN_KEY);
    localStorage.removeItem(USER_ID);
  }

  register(userInfo : Register){
    return this.http.post(`${this.apiUrl}/api/Auth/register`, userInfo);
  }
}
