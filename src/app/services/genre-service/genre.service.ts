import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Genre } from 'src/app/interfaces/genre';
import { GenreAdd } from 'src/app/interfaces/genre-add';
import { CARD_FILE_API_URL } from 'src/app/models/app-injection-tokens';

@Injectable({
  providedIn: 'root'
})
export class GenreService {

  constructor(private http: HttpClient, 
    @Inject(CARD_FILE_API_URL) private apiUrl: string) { }

  getGenreById(id: string):Observable<Genre>{
    return this.http.get<Genre>(`${this.apiUrl}/api/Genre/${id}`);
  }

  getGenres():Observable<Genre[]>{
    return this.http.get<Genre[]>(`${this.apiUrl}/api/Genre`);
  }

  addGenre(genre: GenreAdd):Observable<Genre>{
    return this.http.post<Genre>(`${this.apiUrl}/api/Genre/Add`, genre);
  }

  deleteGenre(genreId: string):Observable<any>{
    return this.http.delete(`${this.apiUrl}/api/Genre/Delete/${genreId}`);
  }
}
