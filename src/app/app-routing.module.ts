import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPanelComponent } from './components/profile/admin-panel/admin-panel.component';
import { ChangePasswordComponent } from './components/profile/change-password/change-password.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RegisterComponent } from './components/register/register.component';
import { TextAddComponent } from './components/text/text-add/text-add.component';
import { AdminGuard } from './guards/admin.guard';
import { IsLoggedGuard } from './guards/is-logged.guard';
import { IsNotLoggedGuard } from './guards/is-not-logged.guard';

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "login", component: LoginComponent, canActivate: [IsLoggedGuard]},
  {path: "register", component: RegisterComponent, canActivate: [IsLoggedGuard]},
  {path: "profile", component: ProfileComponent, canActivate: [IsNotLoggedGuard]},
  {path: "add-text", component: TextAddComponent, canActivate: [IsNotLoggedGuard]},
  {path: "change-password", component: ChangePasswordComponent, canActivate: [IsNotLoggedGuard]},
  {path: "admin-panel", component: AdminPanelComponent, canActivate: [IsNotLoggedGuard, AdminGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
